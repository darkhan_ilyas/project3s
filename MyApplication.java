package com.company;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import java.io.*;
import java.util.Scanner;

public class MyApplication {

    ArrayList<User> users;

    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    public MyApplication(){
        users = new ArrayList<>();
    }

    private void addUser(User user) {
        users.add(user);
    }

    public void start() throws IOException {

        readFile();

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

    }

    private void readFile() throws FileNotFoundException {
        File file = new File("C:\\Users\\malba\\IdeaProjects\\untitlled\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while(fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            String[] data = line.split(" ");
            User clone = new User(Integer.parseInt(data[0]),data[1],data[2],data[3], data[4]);
            users.add(clone);
        }
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else return;
            }
            else {
                userProfile();
            }
        }
    }

    private void authentication() throws IOException {
        while(true){
            System.out.println("1. Sign in");
            System.out.println("2. Sign up");
            System.out.println("3. Exit");
            int choice = sc.nextInt();
            if(choice == 1) signIn();
            else if(choice == 2) signUp();
            else if(choice == 3) return;
        }
    }

    private void signIn() throws IOException {
        while (true){
            System.out.println("Enter your username: ");
            String username = sc.next();
            System.out.println("Enter your password:");
            String passwordStr = sc.next();
            if(checkUser(username, passwordStr)){
                signedUser = getUser(username,passwordStr);
                userProfile();
            }
            else{
                System.out.println("Wrong password or username !!");
            }
        }
    }

    private void signUp() throws IOException {
        while(true) {
            System.out.println("Enter your name: ");
            String name = sc.next();
            System.out.println("Enter your surname: ");
            String surname = sc.next();
            System.out.println("Enter your username: ");
            String username = sc.next();
            if (checkUsername(username)) {
                System.out.println("Enter your password: ");
                String passwordStr = sc.next();
                signedUser = new User(name, surname, username, passwordStr);
                break;
            } else {
                System.out.println("Type another username:");
            }
        }
        addUser(signedUser);
        userProfile();

    }

    private void userProfile() throws IOException {
        while(true) {
            System.out.println("Wellcome" + " " + signedUser.getName() + " " + signedUser.getSurname() + "!!!");
            System.out.println("1. Menu");
            System.out.println("2. Continue as client");
            System.out.println("3. Continue as marketer");
            System.out.println("4. Settings");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            }
            else if (choice == 2){
                asClient();
            }
            else if (choice == 3){
                asMarketer();
            }
            else if (choice == 4){
                Settings();
            }
        }
    }

    public void asClient() throws  IOException {
        while (true) {
            System.out.println("1. Categories-->");
            System.out.println("2. My basket-->");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if(choice == 1 ){
                System.out.println(" There are categories......");
            }
            else if(choice == 2) {
                System.out.println("there are your goods you chosed . . . ");
            }
            else if(choice == 3) {
                userProfile();
            }

        }
    }

    public void asMarketer() throws  IOException {
        while (true) {
            System.out.println("1. Sale-->");
            System.out.println("2. Your sales-->");
            System.out.println("3. Back <--");
            int choice = sc.nextInt();
            if (choice == 1) {
                System.out.println(" There are you can sale smth......");
            } else if (choice == 2) {
                System.out.println("There are goods you are saling . . . ");
            } else if (choice == 3) {
                userProfile();
            }

        }
    }


    public void Settings() throws IOException {
        while(true) {
            System.out.println("1. Change password ");
            System.out.println("2. Log off ");
            int choice = sc.nextInt();
            if(choice == 1 ){
                changePass();
            }
            else if(choice == 2) {
                logOff();
            }
        }
    }

    private void changePass() throws IOException {
        while (true) {
            System.out.println("1. Enter old password: ");
            System.out.println("2. Exit ");
            int choise = sc.nextInt();
            if (choise == 1) {
                String oldPassword = sc.next();
                if (signedUser.getPassword().equals(oldPassword)) {
                    System.out.println("Enter new password: ");
                    String newPassword = sc.next();
                    System.out.println("Repeat password: ");
                    String repeatPassword = sc.next();
                    if (newPassword.equals(repeatPassword)) {
                        signedUser.setPassword(newPassword);
                    } else {
                        System.out.println("Password don't match!");
                    }
                } else {
                    System.out.println("Password incorrect!!!");
                }
            }else if (choise == 2){
                Settings();
            }
        }
    }

    private void logOff() throws IOException {
        signedUser = null;
        saveUserList();
        start();
    }

    private boolean checkUsername(String usernameStr){
        for(User user : users){
            if(user.getUsername().equals(usernameStr)){
                return false;
            }
        }
        return true;
    }

    private boolean checkUser(String username, String password){
        for(User user : users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }

    private User getUser(String username, String password){
        for(User user : users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

    private void saveUserList() throws IOException {
        String content = "";
        for(User user : users){
            content += user + "\n";
        }
        Files.write(Paths.get("C:\\Users\\Asus\\IdeaProjects\\untitlled\\src\\com\\company\\db.txt"), content.getBytes());
    }
}

