package com.company;

import static java.lang.Character.*;

public class Password {
    private String pass;

    public Password(String pass){
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String Pass) {
        if (checkPassFormat(pass)) {
            this. pass = pass;
        } else {
            System.out.println("Incorrect password format : " + pass);
        }
    }

    private static boolean checkPassFormat(String pass) {
        int len = pass.length();
        boolean hasDig = false;
        boolean hasUp = false;
        boolean hasDow = false;
        if(len > 8 ){
            for (int i=0; i< len;i++){
                if(isDigit(pass.charAt(i))){
                    hasDig = true;
                }
                else if (isLetter(pass.charAt(i))){
                       if(isUpperCase(pass.charAt(i))){
                           hasUp = true;
                    }else
                        hasDow = true;
                }

            }
        }
        if(hasDig && hasUp && hasDow)
            return true;
        return false;
    }
}
